# FX-CG[UI]

This is a user interface layer for emulation of the Casio Prizm graphing
calculator. It is designed to interoperate with
[FX-CG\[CL\]](https://bitbucket.org/tari/fx-cg-cl/) but should be adaptable
to other uses with relative ease.

# Prerequisites

The UI is implemented in Python with Qt. It currently *requires Python 3* and
will not work under Python2. You will also need PyQt to build and run. On
Debian derivatives, these should be provided by the `python3-pyqt4` and
`pyqt4-dev-tools` packages. On Windows, your best bet is the official
[PyQt installer](http://www.riverbankcomputing.com/software/pyqt/download).

## Build

The UI components of the program must be compiled before running the program.
Simply executing `make` in the root source directory will do the necessary
compilation.

Optionally, you may wish to install the code into your Python installation's
site libraries:

    python3 setup.py install

This will build a `fxui` script which may be run from anywhere to launch
the program.

# Running

When running the UI, you must pass a single parameter giving the name of the
shared memory segment to map. On Windows, this may be any string as long as
it contains no backslashes. On other systems, this must specify a file which
is at least 165908 bytes long and is writable by the simulator.

If you opted to install the program with `setup.py`, running `fxui` with the
shared memory parameter should suffice. Otherwise, execute the module
`fx_cg_ui.core` with your Python interpreter (this must be on your PYTHONPATH,
easiest to ensure by running from the FX-CG[UI] source directory).

For example:

    $ dd if=/dev/zero of=IPC.dat bs=1 count=165908
    165908+0 records in
    165908+0 records out
    165908 bytes (166 kB) copied, 0.3284 s, 505 kB/s
    
    # If you installed the UI script..
    $ fxui IPC.dat
    # Otherwise..
    $ python3 -m fx_cg_ui.core IPC.dat

The rather clumsy requirements for specifying shared memory mappings will
likely be lifted in future versions; patches are welcome.

## Connecting FX-CG[CL]

Programs compiled against FX-CG[CL] expect to receive the name of the IPC
mapping on their command line in the same fashion as this program. If
`myprogram` is built against FX-CG[CL]:

    $ fxui IPC.dat
    $ myprogram IPC.dat

This should suffice to connect the program to the UI.
