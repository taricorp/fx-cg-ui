COMPILED_UI := fx_cg_ui/GUI.py fx_cg_ui/GUI_rc.py
all: $(COMPILED_UI)

UIC := pyuic4
RCC := pyrcc4 -py3

fx_cg_ui/%.py : %.ui
	$(UIC) -o $@ $<

fx_cg_ui/%_rc.py : %.qrc
	$(RCC) -o $@ $<

clean:
	rm -f $(COMPILED_UI)
