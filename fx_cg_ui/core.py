import array, mmap, os, struct, sys
from PyQt4.QtGui import *
from PyQt4.QtCore import *

from .GUI import Ui_MainWindow
from . import GUI_rc

def createSharedMapping(mapping):
    # Keyboard registers, VRAM, VRAM mutex
    # 165888 = 384 * 216 * 2
    SHM_SIZE = struct.calcsize("8H165888pI")
    with open(mapping, 'r+b') as f:
        if os.name == 'nt':
            return mmap.mmap(f.fileno(), SHM_SIZE, tagname=mapping)
        elif os.name == 'posix':
            return mmap.mmap(f.fileno(), SHM_SIZE)
        else:
            raise OSError('mmap is not available on this platform!')

# These raw codes come from fxcg/keyboard.h: KEY_PRGM_*
# Ordering here comes from the physical layout on the calculator, left-to-right
# and top-to-bottom.
# Idea for user documentation: a javascripty web page in which the binding
# (and key codes) pop up when a key is hovered on an image of the calculator.
# Possibly even highlight the key on a generic keyboard image (assuming US
# keymap).
kbdregmap = {
    # TODO some keys have semantic overloads based on modifier keys (shift for
    # numbers=>symbols, basically). Need mappings for those. Also for the few
    # symbols in use (period, etc).
    # Double-mapping digits and letters for the number keys would make sense.
    Qt.Key_F1: 79,          # F1
    Qt.Key_F2: 69,          # F2
    Qt.Key_F3: 59,          # F3
    Qt.Key_F4: 49,          # F4
    Qt.Key_F5: 39,          # F5
    Qt.Key_F6: 29,          # F6
    Qt.Key_Shift: 78,       # SHIFT
    # OPTN, VARS
    Qt.Key_Alt: 48,         # MENU
    Qt.Key_Left: 38,        # <direction left>
    Qt.Key_Up: 28,          # <direction up>
    Qt.Key_Control: 77,     # ALPHA
    # x^2, ^
    Qt.Key_Escape: 47,      # EXIT
    Qt.Key_Down: 37,        # <direction down>
    Qt.Key_Right: 27,       # <direction right>
    Qt.Key_A: 76,           # X,\theta,T
    Qt.Key_B: 66,           # log
    Qt.Key_C: 56,           # ln
    Qt.Key_D: 46,           # sin
    Qt.Key_E: 36,           # cos
    Qt.Key_F: 26,           # tan
    Qt.Key_G: 75,           # <partial fraction>
    Qt.Key_H: 65,           # F<->D
    Qt.Key_I: 55,           # (
    Qt.Key_J: 45,           # )
    Qt.Key_K: 35,           # ,
    Qt.Key_L: 25,           # ->
    Qt.Key_M: 74,
    Qt.Key_7: 74,           # 7
    Qt.Key_N: 64,
    Qt.Key_8: 64,           # 8
    Qt.Key_O: 54,
    Qt.Key_9: 54,           # 9
    Qt.Key_Delete: 44,      # DEL
    Qt.Key_Pause: 10,       # AC/ON (exception to the pattern)
    Qt.Key_P: 73,
    Qt.Key_4: 73,           # 4
    Qt.Key_Q: 63,
    Qt.Key_5: 63,           # 5
    Qt.Key_R: 53,
    Qt.Key_6: 53,           # 6
    Qt.Key_S: 43,           # x (multiply)
    Qt.Key_T: 33,           # <divide>
    Qt.Key_X: 72,
    Qt.Key_1: 72,           # 1
    Qt.Key_V: 62,
    Qt.Key_2: 62,           # 2
    Qt.Key_W: 52,
    Qt.Key_3: 52,           # 3
    Qt.Key_X: 42,
    Qt.Key_Plus: 42,        # +
    Qt.Key_Y: 32,
    Qt.Key_Minus: 32,       # -
    Qt.Key_Z: 71,
    Qt.Key_0: 71,           # 0
    Qt.Key_Space: 61,
    Qt.Key_Period: 61,      # .
    Qt.Key_Apostrophe: 51,  # EXP
    Qt.Key_Backslash: 41,   # (-)
    Qt.Key_Return: 31,      # EXE
}
# So now we translate those codes to keyboard register locations.
# row = code % 10
# col = (code / 10) - 1
# key = keyregs[row] & (1 << col)

class FXUI(QMainWindow, Ui_MainWindow):
    def __init__(self, mapping):
        # Tool style is what I want, but keeps the program from exiting when closed.
        #QDialog.__init__(self, flags=QtCore.Qt.Tool)
        QDialog.__init__(self, flags=Qt.WindowTitleHint |
                                     Qt.WindowSystemMenuHint)
        self.setupUi(self)
        # Partition the IPC memory block with views for ease of use
        self.shm = memoryview(createSharedMapping(mapping))
        self.vram = self.shm[16:-4]
        self.keyboard = self.shm[:16]

        self.screenRefresher = t = QTimer(self)
        t.setInterval(1000 / 30)
        t.timeout.connect(self.updateDisplay)
        t.start()

        ## Screen actions
        self.action_SaveImage = action = QAction('Save screen image..', self)
        action.setShortcut('Ctrl+S')
        action.triggered.connect(self.saveScreenImage)
        self.CGdisplay.addAction(action)
        action = QAction('-- Separator --', self)
        action.setSeparator(True)
        self.CGdisplay.addAction(action)

        self.displayScale = 1
        scaleAG = QActionGroup(self)
        # We need to force the setter functions to bind to their own closures.
        # Putting the definition of f() in the loop below binds each function
        # to the same integer instance which merely gets mutated.
        # http://stackoverflow.com/questions/233673/lexical-closures-in-python
        def factory(x):
            def f():
                self.displayScale = x
            return f
        for x in range(1,4):
            action = QAction(str(x) + 'x Scale', scaleAG)
            action.setShortcut('Ctrl+' + str(x))
            action.setCheckable(True)
            action.setChecked(x == 1)
            action.triggered.connect(factory(x))
            self.CGdisplay.addAction(action)

        # This does bad things with some window managers. Doesn't appear to be
        # necessary anyway.
        #self.grabKeyboard()

    def translateKeyCode(self, evt):
        # Return keyboard register offset and mask for the key
        if evt.key() not in kbdregmap:
            return (0, 0)
        code = kbdregmap[evt.key()]
        row = code % 10
        col = (code // 10) - 1
        return (row, 1 << col)
    def keyPressEvent(self, evt):
        idx, mask = self.translateKeyCode(evt)
        self.keyboard[idx] |= mask
    def keyReleaseEvent(self, evt):
        idx, mask = self.translateKeyCode(evt)
        self.keyboard[idx] &= ~mask

    def set_displayScale(self, x):
        self._displayScale = x
        self.setFixedSize(x * 384, x * 216)
    def get_displayScale(self):
        return self._displayScale
    displayScale = property(get_displayScale, set_displayScale)

    def saveScreenImage(self):
        """Prompts the user for a filename and saves the current screen image."""
        types = ';;'.join(["Portable Network Graphics (*.png)",
                           "Windows Bitmap (*.bmp)",
                           "Portable Pixmap (*.ppm)"])
        filename = QFileDialog.getSaveFileName(self, filter=types)
        if len(filename) == 0:
            return
        self.CGdisplay.pixmap().save(filename, "PNG")

    @pyqtSlot(QImage)
    def updateDisplay(self):
        img = QImage(self.vram, 384, 216, 384 * 2, QImage.Format_RGB16)
        self.CGdisplay.setPixmap(QPixmap.fromImage(
            img.scaledToHeight(216 * self.displayScale)))

def main():
    import sys
    if len(sys.argv) < 2:
        print("IPC mapping must be passed as first argument.")
        sys.exit(1)
    app = QApplication(sys.argv)
    ui = FXUI(sys.argv[1])
    ui.show()
    sys.exit(app.exec_())
    
if __name__ == "__main__":
    main()
