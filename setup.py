from setuptools import setup, find_packages
setup(
    name = "FX-CG[UI]",
    version = "0.1",
    py_modules = ['fx_cg_ui.core', 'fx_cg_ui.GUI', 'fx_cg_ui.GUI_rc'],
    entry_points = {
        'gui_scripts': [
            'fxui = fx_cg_ui.core:main'
        ]
    },
)
